
import 'package:flutter/material.dart';
import 'package:untitled/fonction/Fonction_static.dart';
import 'package:untitled/model/File.dart';
import 'package:video_player/video_player.dart';

class page_video extends StatelessWidget {
  File file;
  page_video({required this.file});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: VideoPlayerScreen(file: file,),
    );
  }

}

class VideoPlayerScreen extends StatefulWidget{

  File file;
  VideoPlayerScreen({required this.file});

  @override
  State<StatefulWidget> createState() => VideoPlayerScreenState(fichier: file);
}

class VideoPlayerScreenState extends State<VideoPlayerScreen> {

  late File fichier;

  late VideoPlayerController _playerController ;
  String url = Fonction_static.url;
  VideoPlayerScreenState({required this.fichier}){
    url = url + '/stream/' + fichier.nom;
  }

  void initState(){
    _playerController = VideoPlayerController.network(url)
      ..initialize().then((_) {
        setState(() {

        });
      } );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _playerController.value.isInitialized
                ? AspectRatio(
          aspectRatio: _playerController.value.aspectRatio,
          child: VideoPlayer(_playerController),
        ): CircularProgressIndicator() ,

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            if (_playerController.value.isPlaying){
              _playerController.pause();
            } else {
              _playerController.play();
            }
          });
        },
        child: Icon(
          _playerController.value.isPlaying?Icons.pause : Icons.play_arrow,
        ),
      ),
    );
  }
  @override
  void dispose(){
    super.dispose();
    _playerController.dispose();
  }

}
