import 'package:flutter/material.dart';
import 'package:untitled/page/acceuille.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      // home: const MyHomePage(title: 'Stream'),
      debugShowCheckedModeBanner: false,
      home: Acceuille(),
    );
  }
}

