import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:untitled/pagevideo.dart';
import '../fonction/Fonction_static.dart';
import '../model/File.dart';

class Acceuille extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => Acceuille_state();
}

class Acceuille_state extends State<Acceuille> {

  bool isClicqued = false;
  double gauche = 100 ;
  List<File> liste_file = [] ;
  String path = " ";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    change();
    actualiser();
  }

  void change(){
    setState(() {
      isClicqued == true?false:true;
      if (gauche == 0 ){
        gauche = (MediaQuery.of(context).size.width - 100)* -1 ;
      } else {
        gauche = 0 ;
      }
      actualiser();
    });
  }

  void actualiser() {
    Future <List<File>> future = Fonction_static.getDefaultContent() ;
    liste_file = [];
    future.then((value) {
      setState(() {
        liste_file = value;
      });
    });
  }

  void cd (File file){
    Future<List<File>> future = Fonction_static.getcontente(file.nom);
    future.then((value) {
      setState(() {
        liste_file = value;
      });
    });
  }

  void back(){
    Future<List<File>> future = Fonction_static.retoure();
    future.then((value) {
      setState(() {
        liste_file = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation ;
    double taille_font = orientation == Orientation.portrait?100:50 ;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Stream",
          style: TextStyle(
            fontSize: orientation == Orientation.portrait?50:25
          ),
        ),
        centerTitle: orientation == Orientation.portrait?true:false ,
        toolbarHeight: taille_font,
        backgroundColor: Colors.white,
        leading: Container(
          child: Center(
            child: IconButton(
                onPressed: (){
                  change();
                } ,
                icon:const  Icon(Icons.menu)
            )
          ),
        ),
      ),

      body: GestureDetector(
        onHorizontalDragEnd: (details){
          back();
        },
        child: Center (
          child: Stack(
            children: [
              Container(
                height: 800,
                width: MediaQuery.of(context).size.width ,
                decoration: const BoxDecoration(
                  color: Colors.white
                ),
                child: Center(
                  child: ListView.builder(
                    itemCount: liste_file.length ,
                    scrollDirection: Axis.vertical ,
                    itemBuilder: ( BuildContext context,int index) {
                      final painte_text = TextPainter(text: TextSpan(text: liste_file[index].nom),);
                      return  Padding(
                        padding: const EdgeInsets.only(left: 10,top: 10,right: 20),
                        child: Container(
                          height: 100,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black38 ,
                              width: 5,
                            ),
                            color: Colors.black12 ,
                            borderRadius: BorderRadius.circular(10)
                          ),
                          child: TextButton(
                            onPressed: (){
                              if(liste_file[index].type=="fichier"){
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => page_video(file: liste_file[index])
                                  )
                                );
                              } else {
                                cd(liste_file[index]);
                              }
                            },
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: 5,right: 10),
                                  child: Image.memory(
                                    liste_file[index].bytes,
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                                Container(
                                  width: 200,
                                  child: liste_file[index].nom.length > 200?Marquee(
                                    text: liste_file[index].nom ,
                                    scrollAxis: Axis.horizontal,
                                    blankSpace: 50,
                                    startPadding: 10,
                                    pauseAfterRound: const Duration(seconds: 1),
                                  ) : Text(liste_file[index].nom) ,
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                  ),
                ) ,
              ),
              AnimatedPositioned(
                duration: Duration(milliseconds: 500) ,
                top: 0,
                left: gauche ,
                bottom: 0,
                child: Container(
                  height: 300,
                  width: MediaQuery.of(context).size.width -  100 ,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: const Center(
                    child: Text("text"),
                  ),
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}







