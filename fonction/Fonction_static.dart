
import 'dart:convert';

import '../model/File.dart';
import '../model/Partition.dart';
import 'package:http/http.dart' as http;

class Fonction_static {

  static final String url = "http://192.168.43.130:8080";

  static Future <void> getPartition(List<Partition> liste) async {
    print('hello world less e!!');
    String urls = url + '/disk';
    final response = await http.get(Uri.parse(urls));

    if (response.statusCode == 200){

    } else {
      print(response.statusCode);
      print(response.body);
    }

    liste = [];
  }

  static Future<List<File>> getdonner (String urls ) async {
    List<File> liste = [];
    liste = [];
    print(urls);
    final response = await http.get(Uri.parse(urls));
    if (response.statusCode == 200){
      // print(response.body);
      final responseBody = utf8.decode(response.bodyBytes) ;
      List resp = json.decode(responseBody);
      liste = [];
      resp.forEach((element) {
        // print(element);
        liste.add(new File(element['nom'], element['type'], element['path'], element['vignette'] ));
      });
      return liste;
    } else {
      print(response.statusCode);
      print(response.body);
    }
    return [];
  }

  static Future<void> restore() async {
    String urls = url + '/restor';
    await http.get(Uri.parse(urls));
  }

  static Future<List<File>> retoure () async {
    String urls = url + '/back';
    return getdonner(urls);
  }

  static Future<List<File>> getcontente (String nom) async {
    String urls = url + '/file/'+nom;
    return getdonner(urls);
  }

  static Future<List<File>> getDefaultContent() async {
    String urls = url + '/file_default';
    return getdonner(urls);
  }

  static Future<List<File>> getfile () async {
    String urls = url + '/file_default';
    List<File> liste = [];
    print(urls);
    final response = await http.get(Uri.parse(urls));
    if (response.statusCode == 200){
      List resp = json.decode(response.body);
      liste = [];
      resp.forEach((element) {
        liste.add(new File(element['nom'], element['type'], element['path'],element['vignette'] ));
      });
      return liste;
    } else {
      print(response.statusCode);
      print(response.body);
    }

    return [];
  }

}

